def Calculadora_Clasica():
    print("Usted ha elegido la opción:'Calculadora Clásica'")
    num1 = float(input("Ingrese el primer numero de la operación: "))
    resultado = num1
    operando = input("Ingrese el operando deseado ('+', '-', '*', o '/'): ")
    
    while operando == "+":
        num2 = float(input("Ingrese el siguiente número de la operación: "))
        resultado = resultado + num2
        operando = input("Ingrese '+' para seguir sumando, '=' para mostrar el resultado")
    
    while operando == "-":
        num2 = float(input("Ingrese el siguiente número de la operación: "))
        resultado = resultado - num2
        operando = input("Ingrese '-' para seguir restando, '=' para mostrar el resultado")
    
    while operando == "*":
        num2 = float(input("Ingrese el siguiente número de la operación: "))
        resultado = resultado * num2
        operando = input("Ingrese '*' para seguir multiplicando, '=' para mostrar el resultado")
        
    while operando == "/":
        num2 = float(input("Ingrese el siguiente número de la operación: "))
        while num2 == 0:
            num2 = float(input("Numero invalido"))
        resultado = resultado / num2
        operando = input("Ingrese '/' para seguir dividiendo, '=' para mostrar el resultado")
    if operando == "=":
        return print (resultado) 
    else: 
        print("Operando invalido")
    #return print("Cuenta realizada")
    
def Calculadora_Fraccion():
    print("Usted ha elegido la opción:'Calculadora de Fracciones'")
    num1 = int(input("Ingrese el primer numerador: "))
    den1 = int(input("Ingrese el primer denominador: "))
    while den1 == 0:
        den1 = int(input("Ingrese un numero valido: "))
    num2 = int(input("Ingrese el segundo numerador: "))
    den2 = int(input("Ingrese el segundo denominador: "))
    while den2 == 0:
        den2 = int(input("Ingrese un numero valido: "))

    operando = input("Ingrese el operando deseado ('+', '-', '*', o '/'): ")

    if operando == "+":
        if den1 != den2: #SI SON DISTINTOS DENOMINADORES
            comun_den = den1 * den2 #TOTAL DENOMINADOR
            num1 = num1 * den2
            num2 = num2 * den1
            tot_num = num1 + num2 #TOTAL NUMERADOR
            #print(f"{tot_num} / {comun_den}")
        else: #SI SON IGUALES
            comun_den = den1
            tot_num = num1 + num2 #TOTAL NUMERADOR
            #print(f"{tot_num} / {den1}")
    elif operando == "-":
        if den1 != den2: #SI SON DISTINTOS DENOMINADORES
            comun_den = den1 * den2 #TOTAL DENOMINADOR
            num1 = num1 * den2
            num2 = num2 * den1
            tot_num = num1 - num2 #TOTAL NUMERADOR
            #print(f"{tot_num} / {comun_den}")
        else: #SI SON IGUALES
            comun_den = den1
            tot_num = num1 - num2 #TOTAL NUMERADOR
            #print(f"{tot_num} / {den1}")
    elif operando == "*":
        tot_num = num1 * num2
        comun_den = den1 * den2
        #print(f"{tot_num} / {comun_den}")
    elif operando == "/":
        tot_num = num1 * den2
        comun_den = num2 * den1
        #print(f"{tot_num} / {comun_den}")
    else:
        print("Operando invalido")
    return print(f"{tot_num} / {comun_den}")

def Calculadora_Conversion():
    print("Usted ha elegido la opción:'Calculadora de Conversión'")
    resto = 0
    num_bin = 0
    num_oct = 0
    num_hex = 0
    num_HEX = 0
    
    cociente = int(input("Ingrese un numero entero positivo de 4 digitos: "))
    while cociente > 9999 or cociente < 0:
        cociente = int(input("Ingrese un numero valido:"))
    
    opcion = input("Ingrese 'bin','oct', o 'hex'\n")
    if opcion.lower() == "bin":
        cociente1 = cociente
        while cociente >= 2:
            num_bin = str(num_bin) + str(cociente%2) #ACUMULADOR RESTO
            cociente = cociente // 2 #CUENTA COCIENTE 
        num_BIN = str(cociente) + num_bin[::-1] 
        num_BIN = num_BIN[0:-1] #da vuelta el numero de restos
        print(f"{cociente1}(dec) = {num_BIN}(bin)")
    elif opcion.lower() == "oct":
        cociente1 = cociente
        while cociente >= 8:
            num_oct = str(num_oct) + str(cociente%8) #ACUMULADOR RESTO
            cociente = cociente // 8 #CUENTA COCIENTE 
        num_OCT = str(cociente) + num_oct[::-1] 
        num_OCT = num_OCT[0:-1] #da vuelta el numero de restos
        print(f"{cociente1}(dec) = {num_OCT}(oct)")
    elif opcion.lower() == "hex":
        cociente1 = cociente
        while cociente >= 16: 
            num_HEX= str(cociente%16) + str(num_HEX) #Acumulador resto  
            cociente = cociente // 16 #CUENTA COCIENTE
        num_HEX = num_HEX[0:-1] #da vuelta el numero de restos
        num_HEX= num_HEX.replace("10", "A").replace("11", "B").replace("12", "C").replace("13", "D").replace("14", "E").replace("15", "F")
        num_HEX = str(cociente) + num_HEX #añade el cociente al principio 
        print(f"{cociente1}(dec) = {num_HEX}(hex)")
    else:
        print("Base no valida")
    return 
    
encendido = "OFF"
    
while encendido.upper() != "ON":
    encendido = input("Ingrese ON para encender la calculadora")
while encendido.upper() =="ON":
    
    print("Opción 1: Calculadora Clásica")
    print("Opción 2: Calculadora de Fracciones")
    print("Opción 3: Calculadora de Conversiones")
    print("Opción 4: Apagar la calculadora")
    opcion_calc = input("Ingrese la opción que elija: ")
    if opcion_calc == "1":
        Calculadora_Clasica()
    elif opcion_calc == "2":
        Calculadora_Fraccion()
    elif opcion_calc == "3":
        Calculadora_Conversion()
    elif opcion_calc == "4":
        encendido = "OFF"
    else:
        print ("Opcion no valida")